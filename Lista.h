#pragma once

#include "stdafx.h"
#include <iostream>

using namespace std;

struct element {
	element *nastepny;
	int wartosc;
	element();  // Konstruktor
};
element::element() {
	nastepny = NULL;
}
class Lista {
private:
	element *pierwszy;			    // Wskaznik na pierwszy element listy
	element *ostatni;			// Wskaznik na przedostatni element listy
public:
	Lista()
	{
		pierwszy = NULL;
		ostatni = NULL;
	} 
	int rozmiar() {
		element *temp = getPierwszy();
		if (getPierwszy() == NULL) return 0;
		int i;
		for (i = 1; temp->nastepny; i++) {
			temp = temp->nastepny;
		} return i;
	}
	element* getPierwszy() { return pierwszy; }
	void setPierwszy(element *pierwszy) { this->pierwszy = pierwszy; }
	element* getOstatni() { return ostatni; }
	void setOstatni(element *ostatni) { this->ostatni = ostatni; }
	void DodajPoczatek(int wartosc);
	void DodajKoniec(int wartosc);
	void Wyswietl();
	void Usun(int n);
	void UsunPoczatek();
	void WyczyscListe();
};

void Lista::DodajPoczatek(int wartosc) {
	element *nowy = new element;
	nowy->wartosc = wartosc;

	if (getPierwszy() == NULL) {
		setPierwszy(nowy);
		setOstatni(nowy);
	}
	else {
		element *temp = getPierwszy();
		setPierwszy(nowy);
		getPierwszy()->nastepny = temp;
	}
}
void Lista::DodajKoniec(int wartosc) {
	element *nowy = new element;
	nowy->wartosc = wartosc;
	if (getOstatni() == NULL) {
		setOstatni(nowy);
		setPierwszy(nowy);
	}
	else {
		element *temp = getOstatni();
		setOstatni(nowy);
		temp->nastepny = nowy;
	}
}
void Lista::Wyswietl() {
	element *temp = getPierwszy();
	while (temp) {
		cout << temp->wartosc << "\t";
		temp = temp->nastepny;
	}
	cout << endl;
}
void Lista::Usun(int n) {
	element *temp = getPierwszy();

	if (n == 1) {									  // Usuwanie pierwszego elementu
		if (temp->nastepny) setPierwszy(temp->nastepny);
		else {
			delete pierwszy;
			setPierwszy(NULL);						  // Jesli jest tylko 1 element na liscie
			return;
		}
	}
	if (n > 0) {
		for (int j = 0; j < n; j++) {				  // Usuwanie srodkowego elementu
			if (n - j == 1) break;                    // Wyjdz jesli znajdziesz element n-1
			if (temp) temp = temp->nastepny;
			else {									  // Jesli n jest wieksze niz dlugosc listy
				cerr << "Przekroczenie zakresu listy!" << endl;
				return;
			}
		}
		element *temp2 = temp->nastepny;              // chemy go usunac
		if (temp->nastepny->nastepny) temp->nastepny = temp->nastepny->nastepny;    // element n-1 wskazuje na element n+1
		else temp->nastepny = NULL;
		delete temp2;
	}
}
void Lista::UsunPoczatek() {
	element *temp = getPierwszy()->nastepny;
	delete getPierwszy();
	setPierwszy(temp);
}
void Lista::WyczyscListe() {
	element *temp = pierwszy;

	while (getPierwszy()) {
		temp = getPierwszy();
		setPierwszy(getPierwszy()->nastepny);
		delete temp;
	}
}