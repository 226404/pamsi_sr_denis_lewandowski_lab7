// Tablica Haszujaca.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Tablica Haszujaca.h"
#include <iostream>
#include <ctime>
#include "Lista.h"

using namespace std;

bool czyPierwsza(int x) {
	int N = sqrt(x);
	for (int i = 2; i <= N; i++)
		if (x%i == 0) return false;
	return true;
}

int main()
{
	int N;					// Rozmiar tablicy haszujacej
	int x;					// Zmienna przytrzymujaca wylosowana wartosc
	int *randTab;			// Tablica do przechowywania losowych liczb bez powtorzen
	bool kolizja = false;
	srand(time(NULL));

	// Tworzenie Tablicy Haszujacej
	cout << "*****************************Tablica Haszujaca****************************" << endl;
	while(true) {
		cout << "Wprowadz ilosc elementow do wstawienia (musi byc to liczba pierwsza): ";
		cin >> N;
		if (czyPierwsza(N))
		{
			cout << "Wczytano pomyslnie!" << endl;
			break;
		}
		cout << "Liczba " << N << " nie jest liczba pierwsza!" << endl;
	}

	HashTab tab(N);
	randTab = new int[N];

	// Losowanie liczb bez powtorzen i wrzucanie ich na tablice
	for (int i = 0; i < N; i++)
	{
		x = rand()%20;
		randTab[i] = x;
		for (int j = 0; j < i; j++)
		{
			if (randTab[j] == x) i--; // Cofa licznik petli zewnetrznej gdy liczba sie powtorzyla
		}
	}




	// Probkowanie liniowe
	cout << endl << "Dodawanie elementow - probkowanie liniowe: " << endl;
	for (int i = 0; i < N; i++)
	{
		tab.dodaj(randTab[i]);
		cout << endl;
	}

	cout << endl << "Znajdz: " << tab.znajdz(12) << endl;
	cout << endl << "Usuwanie!" << endl;
	tab.usun(4);
	
	cout << endl << endl;


	// Linkowanie
	HashTab2 hashTab2(N);

	cout << "**********Linkowanie***********" << endl;

	hashTab2.dodaj2(10);
	cout << "Element listy: " << hashTab2[10].getPierwszy()->wartosc << endl;
	cout << "Usuwanie elementu!" << endl;
	hashTab2.usun2(10);
	if( hashTab2.znajdz2(10) == NULL) cout << "Nie znaleziono elementu!" << endl;



	// Podwojne haszowanie

	HashTab3 hashTab3(N);

	cout << "***********Podwojne Haszowanie************" << endl << endl;

	cout << endl << "Dodawanie elementow " << endl;
	for (int i = 0; i < N; i++)
	{
		hashTab3.dodaj(randTab[i]);
		cout << endl;
	}

	cout << endl << "Funkcja znajdz: " << endl;
	hashTab3.znajdz(10);

	cout << endl << "Usuwanie elementu 10: " << endl;
	hashTab3.usun(10);


	getchar();
	getchar();
    return 0;
}
