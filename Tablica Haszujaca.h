#pragma once
#include <iostream>
#include "Lista.h"

using namespace std;


class HashTab {

	int *Tab;	
	int N;		// rozmiar tablicy haszujacej

public:
	HashTab(int N) {
		this->N = N;
		Tab = new int[N];
		for (int i = 0; i < N; i++)
			Tab[i] = NULL;
	}
	int h(int x);			// Funkcja haszujaca
	int & operator [] (int E) { return Tab[E]; }
	void dodaj(int x);
	int znajdz(int x);
	void usun(int x);
	
};

class HashTab2 {

	Lista *Tab;
	int N;		// rozmiar tablicy haszujacej

public:
	HashTab2(int N) {
		this->N = N;
		Tab = new Lista[N];
	}
	int h(int x)		// Funkcja haszujaca
	{			
		return x % N;
	}						
	Lista & operator [] (int E) { return Tab[E]; }
	void dodaj2(int x);
	element* znajdz2(int x);
	void usun2(int x);

};

class HashTab3 {

	int *Tab;
	int N;		// rozmiar tablicy haszujacej

public:
	HashTab3(int N) {
		this->N = N;
		Tab = new int[N];
		for (int i = 0; i < N; i++)
			Tab[i] = NULL;
	}
	int h(int x) { return x % N; }			// Pierwsza funkcja haszujaca
	int d(int x) { return 11 - (x % 11); }	// Druga    funkcja haszujaca

	int & operator [] (int E) { return Tab[E]; }
	void dodaj(int x);
	int znajdz(int x);
	void usun(int x);

};


int HashTab::h(int x)
{
	return x % N;
}

void HashTab::dodaj(int x)
{
	int i = 0;
	cout << "Liczba " << x << "\tProbki: ";
	while (Tab[(h(x) + i)%N] != NULL)
	{
		cout << (h(x) + i) % N << " ";
		i++;
	}
	cout << (h(x) + i) % N;
	Tab[(h(x) + i) % N] = x;
}

inline int HashTab::znajdz(int x)
{
	if (Tab[h(x)] == NULL)
	{
		return NULL;
	}
	else
	{
		cout << "Poszukiwana liczba: " << x << "\tProbki: ";
		for (int i = 0; i < N; i++)
		{
			cout << (h(x) + i) % N << " ";
			if (Tab[(h(x) + i) % N] == NULL) return NULL;
			if (Tab[(h(x) + i) % N] == x) return (h(x) + i) % N;
		}
	}
	return NULL;
}

inline void HashTab::usun(int x)
{
	int tmp = znajdz(x);
	if (tmp == NULL)
	{
		cout << endl << "Nie ma takiego elementu w tablicy!" << endl;
		return;
	}
	else
	{
		cout << endl << "Usunieto element o indeksie " << tmp << endl;
		Tab[tmp] = NULL;
	}
}

void HashTab2::dodaj2(int x)
{
		Tab[h(x)].DodajKoniec(x);
}

inline element* HashTab2::znajdz2(int x)
{
	if (Tab[h(x)].rozmiar() == 0) 
		return NULL;
	element *tmp;
	tmp = Tab[h(x)].getPierwszy();
	do {
		if (tmp->wartosc == x) return tmp;

		if (tmp->nastepny != NULL) tmp = tmp->nastepny;
		else break;
	} while (tmp);
	return NULL;
}

inline void HashTab2::usun2(int x)
{
	element *tmp;
	tmp = Tab[h(x)].getPierwszy();
	int i = 1;
	while (tmp)
	{
		if (tmp->wartosc == x)
		{
			Tab[h(x)].Usun(i);
			return;
		}
		tmp = tmp->nastepny;
		i++;
	}
}


void HashTab3::dodaj(int x)
{
	int idx;
	cout << "Liczba " << x << "\tProbki: ";
	for (int i = 0; i < N; i++)
	{
		idx = (h(x) + i*d(x)) % N;
		cout << idx << " ";
		if (Tab[idx] == NULL)
		{
			Tab[idx] = x;
			return;
		}
	}
	cout << "Tablica jest pelna!" << endl;
}

inline int HashTab3::znajdz(int x)
{
	int idx;
	idx = (h(x) + 0*d(x)) % N;
	if (Tab[idx] == NULL)
	{
		return NULL;
	}
	else
	{
		cout << "Poszukiwana liczba: " << x << "\tProbki: ";
		for (int i = 0; i < N; i++)
		{
			idx = (h(x) + i*d(x)) % N;
			cout << (idx) % N << " ";
			if (Tab[idx] == NULL) return NULL;
			if (Tab[idx] == x) return idx;
		}
	}
	return NULL;
}

inline void HashTab3::usun(int x)
{
	int tmp = znajdz(x);
	if (tmp == NULL)
	{
		cout << endl << "Nie ma takiego elementu w tablicy!" << endl;
		return;
	}
	else
	{
		cout << endl << "Usunieto element o indeksie " << tmp << endl;
		Tab[tmp] = NULL;
	}
}

